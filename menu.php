<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link <?php if($current_page == "home"){echo 'active';} ?>" href="index.php">Home</a>
      <a class="nav-item nav-link <?php if($current_page == "register"){echo 'active';} ?>" href="register.php">Register</a>
      <a class="nav-item nav-link <?php if($current_page == "about_us"){echo 'active';} ?>" href="about_us.php">About Us</a>
    </div>
  </div>
</nav>