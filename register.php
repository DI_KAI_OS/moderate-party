<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
        <link rel="icon" href="icon.png">
    </head>
    <body>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
        <script>
            function myFunction() {
                var x = document.getElementById("myDIV");
                if (x.style.display === "none") {
                    x.style.display = "block";
                } else {
                    x.style.display = "none";
                }
            }
        </script>
        <?php
            $current_page = 'register';
            include("menu.php");
        ?>
        <div class="container">
            <form class="py-md-3 pl-md-5 bd-content">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>ชื่อ</label>
                        <input type="text" class="form-control" placeholder="ชื่อ">
                    </div>
                    <div class="form-group col-md-6">
                        <label>ชื่อสกุล</label>
                        <input type="text" class="form-control" placeholder="ชื่อสกุล">
                    </div>
                </div>
                <div class="form-group">
                    <label>เลขประจำตัวประชาชน</label>
                    <input type="text" class="form-control" placeholder="เลขประจำตัวประชาชน">
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" value="" name="national" id="national1">
                        <label class="form-check-label" for="national1">
                            สัญชาติไทยโดยการเกิด
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" value="" name="national" id="national2">
                        <label class="form-check-label" for="national2">
                        สัญชาติไทยโดยการแปลงสัญชาติ
                        </label>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-8">
                        <label>วันเกิด</label>
                        <input type="date" class="form-control" placeholder="วันที่">
                    </div>
                    <div class="form-group col-4">
                        <label>ขณะนี้อายุ</label>
                        <div class="mx-auto col" id="age">0</div>
                    </div>
                </div>
                <div class="form-group">
                    <label>สถานที่เกิด</label>
                    <input type="text" class="form-control" placeholder="11/98 ซ.คลองหลวง 11 ถ.พหลโยธิน ม.7 ต.คลองหลวง">
                </div>
                <div class="form-row">
                    <div class="form-group col-5">
                        <label>เขต/อำเภอ</label>
                        <input type="text" class="form-control" placeholder="เขต">
                    </div>
                    <div class="form-group col-4">
                        <label>จังหวัด</label>
                        <input type="text" class="form-control" placeholder="จังหวัด">
                    </div>
                    <div class="form-group col-3">
                        <label>รหัสไปรษณีย์</label>
                        <input type="text" class="form-control" placeholder="รหัสไปรษณีย์">                        
                    </div>
                </div>
                <div class="form-group">
                    <label>ที่อยู่ตามทะเบียนบ้าน</label>
                    <input type="text" class="form-control" placeholder="11/98 ซ.คลองหลวง 11 ถ.พหลโยธิน ม.7 ต.คลองหลวง">
                </div>
                <div class="form-row">
                    <div class="form-group col-5">
                        <label>เขต/อำเภอ</label>
                        <input type="text" class="form-control" placeholder="เขต">
                    </div>
                    <div class="form-group col-4">
                        <label>จังหวัด</label>
                        <input type="text" class="form-control" placeholder="จังหวัด">
                    </div>
                    <div class="form-group col-3">
                        <label>รหัสไปรษณีย์</label>
                        <input type="text" class="form-control" placeholder="รหัสไปรษณีย์">                        
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-3">
                        <label>โทร</label>
                        <input type="text" class="form-control" placeholder="โทร">
                    </div>
                    <div class="form-group col-3">
                        <label>โทรสาร</label>
                        <input type="text" class="form-control" placeholder="โทรสาร">
                    </div>
                    <div class="form-group col-6">
                        <label>E-mail</label>
                        <input type="text" class="form-control" placeholder="E-mail">
                    </div>
                </div>
                <div class="form-group pb-1">
                    <label>อาชีพ</label>
                    <input type="text" class="form-control" placeholder="อาชีพ">
                </div>
                <hr class="py-1">
                <div class="form-row">
                    <div class="form-group col-6">
                        <label>ชื่อบิดา</label>
                        <input type="text" class="form-control" placeholder="ชื่อ">
                    </div>
                    <div class="form-group col-6">
                        <label>ชื่อสกุล</label>
                        <input type="text" class="form-control" placeholder="ชื่อสกุล">
                    </div>
                </div>
                <div class="form-group">
                        <label>สัญชาติ</label>
                    <input type="text" class="form-control" placeholder="สัญชาติ">
                </div>
                <div class="form-row">
                    <div class="form-group col-6">
                        <label>สถานที่เกิด จังหวัด</label>
                        <input type="text" class="form-control" placeholder="สถานที่เกิด จังหวัด">
                    </div>
                    <div class="form-group col-6">
                        <label>ประเทศ</label>
                        <input type="text" class="form-control" placeholder="ประเทศ">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-3">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" value="" name="dad_is_live" id="dad_is_live1">
                            <label class="form-check-label" for="dad_is_live1">
                                ยังมีชีวิตอยู่
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" value="" name="dad_is_live" id="dad_is_live2">
                            <label class="form-check-label" for="dad_is_live2">
                                ตาย
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>อายุ</label>
                        <input type="text" class="form-control" placeholder="อายุ (ปี)">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-6">
                        <label>ชื่อมารดา</label>
                        <input type="text" class="form-control" placeholder="ชื่อ">
                    </div>
                    <div class="form-group col-6">
                        <label>ชื่อสกุล</label>
                        <input type="text" class="form-control" placeholder="ชื่อสกุล">
                    </div>
                </div>
                <div class="form-group">
                        <label>สัญชาติ</label>
                    <input type="text" class="form-control" placeholder="สัญชาติ">
                </div>
                <div class="form-row">
                    <div class="form-group col-6">
                        <label>สถานที่เกิด จังหวัด</label>
                        <input type="text" class="form-control" placeholder="สถานที่เกิด จังหวัด">
                    </div>
                    <div class="form-group col-6">
                        <label>ประเทศ</label>
                        <input type="text" class="form-control" placeholder="ประเทศ">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-3">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" value="" name="mom_is_live" id="mom_is_live1">
                            <label class="form-check-label" for="mom_is_live1">
                                ยังมีชีวิตอยู่
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" value="" name="mom_is_live" id="mom_is_live2">
                            <label class="form-check-label" for="mom_is_live2">
                                ตาย
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>อายุ</label>
                        <input type="text" class="form-control" placeholder="อายุ (ปี)">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label>คู่สมรส ชื่อ</label>
                        <input type="text" class="form-control" placeholder="ชื่อ">
                    </div>
                    <div class="form-group col">
                        <label>ชื่อสกุล</label>
                        <input type="text" class="form-control" placeholder="ชื่อสกุล">
                    </div>
                </div>
                <div class="form-group">
                    <label>สัญชาติ</label>
                    <input type="text" class="form-control" placeholder="สัญชาติ">
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label>สถานที่เกิด จังหวัด</label>
                        <input type="text" class="form-control" placeholder="สถานที่เกิด จังหวัด">
                    </div>
                    <div class="form-group col">
                        <label>ประเทศ</label>
                        <input type="text" class="form-control" placeholder="ประเทศ">
                    </div>
                </div>
                <div class="form-group">
                    <label>อาชีพ</label>
                    <input type="text" class="form-control" placeholder="อาชีพ">
                </div>
                <div class="form-row">
                    <div class="form-group col-3">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" value="" name="mate_is_live" id="mate_is_live1">
                            <label class="form-check-label" for="mate_is_live1">
                                ยังมีชีวิตอยู่
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" value="" name="mate_is_live" id="mate_is_live2">
                            <label class="form-check-label" for="mate_is_live2">
                                ตาย
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>อายุ</label>
                        <input type="text" class="form-control" placeholder="อายุ (ปี)">
                    </div>
                </div>
                <div class="form-group">
                    <label>จำนวนบุตร</label>
                    <input type="text" class="form-control" placeholder="จำนวน (คน)">
                </div>
                <div class="form-row">
                    <div class="form-group col-5">
                        <label>ชื่อ</label>
                        <input type="text" class="form-control" placeholder="ชื่อ">
                    </div>
                    <div class="form-group col-5">
                        <label>ชื่อสกุล</label>
                        <input type="text" class="form-control" placeholder="ชื่อสกุล">
                    </div>
                    <div class="form-group col-2">
                        <label>อายุ</label>
                        <input type="text" class="form-control" placeholder="อายุ">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-5">
                        <label>ชื่อ</label>
                        <input type="text" class="form-control" placeholder="ชื่อ">
                    </div>
                    <div class="form-group col-5">
                        <label>ชื่อสกุล</label>
                        <input type="text" class="form-control" placeholder="ชื่อสกุล">
                    </div>
                    <div class="form-group col-2">
                        <label>อายุ</label>
                        <input type="text" class="form-control" placeholder="อายุ">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-5">
                        <label>ชื่อ</label>
                        <input type="text" class="form-control" placeholder="ชื่อ">
                    </div>
                    <div class="form-group col-5">
                        <label>ชื่อสกุล</label>
                        <input type="text" class="form-control" placeholder="ชื่อสกุล">
                    </div>
                    <div class="form-group col-2">
                        <label>อายุ</label>
                        <input type="text" class="form-control" placeholder="อายุ">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-5">
                        <label>ชื่อ</label>
                        <input type="text" class="form-control" placeholder="ชื่อ">
                    </div>
                    <div class="form-group col-5">
                        <label>ชื่อสกุล</label>
                        <input type="text" class="form-control" placeholder="ชื่อสกุล">
                    </div>
                    <div class="form-group col-2">
                        <label>อายุ</label>
                        <input type="text" class="form-control" placeholder="อายุ">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group">
                        <label>การเป็นสมาชิกพรรคการเมือง</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" value="" name="use_to_member" id="use_to_member1">
                            <label class="form-check-label" for="use_to_member1">
                                ไม่เคยเป็นสมาชิกพรรคการเมืองใดมาก่อน
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" value="" name="use_to_member" id="use_to_member2">
                            <label class="form-check-label" for="use_to_member2">
                                เคยเป็นสมาชิกพรรคการเมืองอื่นมาก่อน
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-5">
                        <label>(๑) พรรค</label>
                        <input type="text" class="form-control" placeholder="พรรค">
                    </div>
                    <div class="form-group col-3">
                        <label>วันที่สมัคร</label>
                        <input type="text" class="form-control" placeholder="วันที่สมัคร">
                    </div>
                    <div class="form-group col-3">
                        <label>วันที่พ้น</label>
                        <input type="text" class="form-control" placeholder="วันที่พ้น">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-5">
                        <label>(๒) พรรค</label>
                        <input type="text" class="form-control" placeholder="พรรค">
                    </div>
                    <div class="form-group col-3">
                        <label>วันที่สมัคร</label>
                        <input type="text" class="form-control" placeholder="วันที่สมัคร">
                    </div>
                    <div class="form-group col-3">
                        <label>วันที่พ้น</label>
                        <input type="text" class="form-control" placeholder="วันที่พ้น">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-5">
                        <label>(๓) พรรค</label>
                        <input type="text" class="form-control" placeholder="พรรค">
                    </div>
                    <div class="form-group col-3">
                        <label>วันที่สมัคร</label>
                        <input type="text" class="form-control" placeholder="วันที่สมัคร">
                    </div>
                    <div class="form-group col-3">
                        <label>วันที่พ้น</label>
                        <input type="text" class="form-control" placeholder="วันที่พ้น">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-5">
                        <label>(๔) พรรค</label>
                        <input type="text" class="form-control" placeholder="พรรค">
                    </div>
                    <div class="form-group col-3">
                        <label>วันที่สมัคร</label>
                        <input type="text" class="form-control" placeholder="วันที่สมัคร">
                    </div>
                    <div class="form-group col-3">
                        <label>วันที่พ้น</label>
                        <input type="text" class="form-control" placeholder="วันที่พ้น">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-5">
                        <label>(๕) พรรค</label>
                        <input type="text" class="form-control" placeholder="พรรค">
                    </div>
                    <div class="form-group col-3">
                        <label>วันที่สมัคร</label>
                        <input type="text" class="form-control" placeholder="วันที่สมัคร">
                    </div>
                    <div class="form-group col-3">
                        <label>วันที่พ้น</label>
                        <input type="text" class="form-control" placeholder="วันที่พ้น">
                    </div>
                </div>
                <div class="form-group">
                    <input class="form-check-input" type="radio" value="" id="accept">
                    <label class="form-check-label" for="accept">
                        ขอแสดงเจตจำนงเป็นผู้ร่วมกันจัดตั้งพรรคการเมือง
                    </label>
                    <label class="form-check-label" for="accept">
                        &ensp;&ensp;ข้าพเจ้าขอรับรองว่าเป็นผู้มีคุณสมบัติและไม่มีลักษณะต้องห้ามตามมาตรา ๙ แห่งพระราชบัญญัติประกอบรัฐธรรมนูญว่าด้วยพรรคการเมือง พ.ศ.๒๕๖๐ และข้อความที่ปรากฎข้างต้นเป็นความจริง
                    </label>
                </div>
            </form>
        </div>
    </body>
</html>
